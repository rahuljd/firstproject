package constructor;
//Example of constructor that displays the constructor
//that displays the default values
public class StudentDC {
int id;
String name;
void display() {
	System.out.println("id=="+id+" name="+name);	
}
public static void main(String[] args) {
	StudentDC s=new StudentDC();
	StudentDC s1=new StudentDC();
	
	s.display();
	s1.display();
}

}
