package ObjectAndClassInitialization;

class Student{
	int id;
	String name;

	public void insert(int i,String s) {
		id=i;
		name=s;
	}
	
	public void display() {
		System.out.println(id+" "+name);
	}

}


public class ThroughMethod {
		public static void main(String[] args) {
			Student s=new Student();
			Student s1=new Student();
			s.insert(10,"rahul");
			s1.insert(20,"rohit");
			
			s.display();
			s1.display();
			
		}
	
}
