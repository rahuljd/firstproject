package ObjectAndClassInitialization;
//maintaning records of employees

class Employees{ 
	int empId;
	String empName;
	int salary;
	
	public void insert(int id,String name,int sal) {
		empId=id;
		empName=name;
		salary=sal;
	}
	
	public void display() {
		System.out.println(empId+" "+empName+" "+salary);
	}
}

public class TestEmployees {
	public static void main(String[] args) {
		Employees e1=new Employees();
		Employees e2=new Employees();
		Employees e3=new Employees();
		
		e1.insert(1,"karan",10000);
		e2.insert(2,"arjun", 20000);
		e3.insert(3,"thakur",30000);
		
		e1.display();
		e2.display();
		e3.display();
		
		
		
	}
}
