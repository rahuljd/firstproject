package ObjectAndClassInitialization;



class AnnonymousObject {	

	
	public void fact(int n) {
	 int f=1;
	 for(int i=1;i<=n;i++) {
		 f=f*i;
	 }
	 System.out.println("factorial of a number"+n+"="+f);
	}
	
	public static void main(String[] args) {
		//annanymous object creation
		
		new AnnonymousObject().fact(6);
		new  AnnonymousObject().fact(7);
		
		
	}
}
