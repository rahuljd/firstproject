package paramerisedConstructor;

public class StudentPC {
int id;
String name;

public StudentPC(int i,String n) {
	id=i;
	name=n;
	}

public void display(){
	System.out.println("student id="+id+" name="+name);
}

public static void main(String[] args) {
	StudentPC s1=new StudentPC(18, "virat");
	StudentPC s2=new StudentPC(45,"rohit");
	s1.display();
	s2.display();

}
}
