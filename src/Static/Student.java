package Static;

public class Student {
int id;
String name;

static String college="MIT";
public Student(int id, String name) {
	
	this.id = id;
	this.name = name;
}

void display() {
	System.out.println("id  "+id+" name "+name+" college "+college);
}

public static void change() {
	college="COEP";
}

public static void main(String[] args) {
Student s=new Student(1,"virat");
Student s2=new Student(2,"rohit");
s.display();
s2.display();

change();

s.display();
s2.display();


}

}
