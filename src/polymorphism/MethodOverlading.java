package polymorphism;

public class MethodOverlading {

	public void test() {
		System.out.println("Empty method");
	}
	
	public void test(int i) {
		System.out.println("accept integer");
		
	}
	
	public void test(double d) {
		System.out.println("accept double");
	}
	
	public void test(int i,double d) {
		System.out.println("accept integer and double");
	}
	
	public static void main(String[] args) {
		MethodOverlading m=new MethodOverlading();
		m.test();
		m.test(0);
		m.test(0.3);;
		m.test(0, 0);
	}
}
