package Inheritance;

public class Student extends Person{
int rollNo;

public Student(String firstNaame, String lastName, int age, String gender, int rollNo) {
	super(firstNaame, lastName, age, gender);
	this.rollNo = rollNo;
}

@Override
public String toString() {
	return "Student [rollNo=" + rollNo + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age
			+ ", gender=" + gender + "]";
}


}
