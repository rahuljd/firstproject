package Inheritance;

public class TestInheritance {

	public static void main(String[] args) {
		Person p=new Person("virat","kohali",29,"m");
		Student s=new Student("y", "chahal",25, "m",23);
		Professor p1=new Professor("ravi","shatri", 59,"m","cricket");
		
		System.out.println(p.toString());
		System.out.println(s.toString());
		System.out.println(p1.toString());
		
		//parent class ref
		Person p3=new Student("rohit ", "sharma",30,"m",45);
		System.out.println("upcasing"+p3.toString());
	}
}
