package Inheritance;

public class Professor extends Person{
String subject;

public Professor(String firstNaame, String lastName, int age, String gender, String subject) {
	super(firstNaame, lastName, age, gender);
	this.subject = subject;
}

@Override
public String toString() {
	return "Professor [subject=" + subject + ", firstName=" + firstName + ", lastName=" + lastName + ", age=" + age
			+ ", gender=" + gender + "]";
}

}
