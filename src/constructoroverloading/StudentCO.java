package constructoroverloading;
//Constructor overloading

public class StudentCO {
int id;
String name;
int age;
public StudentCO(int i,String n) {
id=i;name=n;
}

public StudentCO(int i,String n,int a) {
	id=i;
	name=n;
	age=a;
	
}

public void display() {
	System.out.println("id="+id+" name= "+name+" age="+age);
}


public static void main(String[] args) {
	StudentCO s=new StudentCO(18,"virat");
	StudentCO s2=new StudentCO(45, "rohit",30);
	
	s.display();
	s2.display();
	
}
}
